﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (int.TryParse(input, out result) && (result >= 0))
                return true;
            else
                return false;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] res = new int[n];
            if (n == 0)
                return res;            
            res[0] = 0;
            if (n > 1)
                res[1] = 1;
            if (n <= 2)
                return res;
            for (int i = 2; i < n; ++i)
                res[i] = res[i - 1] + res[i - 2];
            return res;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int res = 0;
            while (sourceNumber != 0)
            {
                res *= 10;
                res += sourceNumber % 10;
                sourceNumber /= 10;
            }
            if (sourceNumber < 0)
                res = -res;
            return res;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] res;
            if (size <= 0)
            {
                res = new int[0];
                return res;
            }
            res = new int[size];
            Random rnd = new Random();
            for (int i = 0; i < size; ++i)
            {
                res[i] = rnd.Next(-100, 100);
            }
            return res;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; ++i)
                source[i] = -source[i];
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] res;
            if (size <= 0)
            {
                res = new int[0];
                return res;
            }
            res = new int[size];
            Random rnd = new Random();
            for (int i = 0; i < size; ++i)
            {
                res[i] = rnd.Next(0, 100);
            }
            return res;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> res = new List<int>();
            for (int i = 1; i < source.Length; ++i)
            {
                if (source[i] > source[i - 1])
                    res.Add(source[i]);
            }
            return res;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int element = 1;
            int[,] res = new int[size, size];
            int step = 0;
            do
            {
                if (!FillHorizontalLine(ref res, ref element, step))
                    break;
                if (!FillVerticalLine(ref res, ref element, step))
                    break;
                if (!FillHorizontalLineBack(ref res, ref element, step))
                    break;
                if (!FillVerticalLineBack(ref res, ref element, step))
                    break;
                ++step;
            }
            while (true);
            return res;
        }

        private bool FillHorizontalLine(ref int[,] source, ref int element, int step)
        {
            for (int i = step; i < (source.GetUpperBound(0) + 1 - step); ++i)
            {
                source[step, i] = element;
                ++element;
                if (element > source.Length)
                    return false;
            }
            return true;
        }
        private bool FillVerticalLine(ref int[,] source, ref int element, int step)
        {
            for (int i = (step + 1); i < (source.GetUpperBound(0) + 1 - step); ++i)
            {
                source[i, (source.GetUpperBound(0) - step)] = element;
                ++element;
                if (element > source.Length)
                    return false;
            }
            return true;
        }
        private bool FillHorizontalLineBack(ref int[,] source, ref int element, int step)
        {
            for (int i = (source.GetUpperBound(0) - step - 1); i >= step; --i)
            {
                source[source.GetUpperBound(0) - step, i] = element;
                ++element;
                if (element > source.Length)
                    return false;
            }
            return true;
        }
        private bool FillVerticalLineBack(ref int[,] source, ref int element, int step)
        {
            for (int i = (source.GetUpperBound(0) - step - 1); i > step; --i)
            {
                source[i, step] = element;
                ++element;
                if (element > source.Length)
                    return false;
            }
            return true;
        }

    }
}
